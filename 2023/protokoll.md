2023-04-09
==========

*(Work-in-Progress, muess no chly ufgrumt wärde)*


Begrüessig
----------

 * Start: 18:30

### Mitglider

Awäsend: 10

 * Chegi
 * Chlöti 1
 * Chlöti 2 (Noel?)
 * Kay
 * Michu
 * Monsi
 * Nädu
 * Nöbsu (Sven)
 * Röifu
 * Tinu

Abgmäudet: 6

 * Claire
 * Meli
 * Päscu
 * Räffu
 * Reto
 * Sämi

Nid entschuudigt: 0

### Protokou/Sekretär

 * Tinu

### Stimmezähler

 * Kay (1 Enthautig, niemer drgäge)

### Traktandelischte

 * … (*siehe dieses Protokoll*)


Rückblick 2022
--------------

### Protokou 2022

(vom Rouf, 19.3.2022)

 * Ufnahme
 * Rouf: Protokou abgäh
 * Konzärt:
    - WG-Party
    - Summerfescht
 * "Schlimme Dinge" definiere: Föteli › Poschtcharte › Ygschribnigs
 * Mitgliderbyträg: 4›5 (wäg de tüürere Bier)

Akzeptiert: 1 drgäge, aui angere drfür

 * Kommentar Nöbsu: "Dr Deep Single Malt kontrolliert aues"
 * Kommentar Chegi: "Perfekt"

### Rückblick vom Chegi

 * Schöns Jahr, Summerfescht isch super gsy, aber chly einsam
 * 2 cooli Uftritte
    - Langethau super
    - Baggwiu o schön
 * Ah ja, no 1 Uftrit meh, bym Reti syre Hochzyt (…)
 * Hei mit de Ufnahme agfange
    - Bis Ändi Jahr öppe 2/3 gha (Chegi)
 * Chläber, u Sache agmaut (z.B. ds Sofa)
 * Bändlogo mit Liecht
 * Rouf het ghürate
 * Dr Umbou het sech bewährt, es het nid brönnt :-D
    - Mir danke am Chef vom Kay
 * Ybruchsserie
    - Bier u di gruusigi Schnapsfläsche gchlauet (Röifu live drby gsy)
    - Gäud (aber unbewise, mir wüsse's nid)
    - Wiudtierkamera installiert, mit em Sändu diskutiert
       * Dr Dechu wird itz äue de eh no ersetzt mit öppis Ybruchssicherem
 * Ds Monsi het öpper kenne glernt, wo ds Totemügerli glych wyt cha wi är


Finanze
-------

### Kassier (Monsi)

 * Zahle: *gemäß Bericht*
    - öppe 950 use
    - öppe 800 yche (150 Franke Verluscht)
    - Houptusgabe:
       * Näbumaschine
       * Sticker
       * Blächlogo
    - Houptynahme:
       * Suufe
       * Uftritte

### Revisor (Rouf)

 * Agluegt, für OK empfunge
    - Kommentar: Revisor sött aber eigentlech o d'Usgabe für d'Zuekunft aluege
    - "Budget-Minus isch ja eh meischtens vo mir cho"
       * Isch aber eh no viu Eigekapitau drinne

```
››› augemeini Piselipouse: Rouf, Nädu, Tin-
```

 * Vorschlag Chegi: Mir bruche e Cash-Cow

### Abnahm

 * Agnoh (1 Enhautig, aui angere drfür)


Vorstand
--------

### Aute Vorstand

Diskussionslos akzeptiert

 * El presidente: Chegi
 * El presidente #2: Rouf
 * Finanze: Monsi
 * Revisor: Rouf (übernoh vom Tinu)
 * Sekretär: Tinu (übernoh vom Rouf)

### Nöie Vorstand

 * El presidente: Chegi agnoh (9 drfür, Chegi enthautet sech)
 * El presidente #2: Rouf agnoh (10 drfür (o dr Rouf))
 * Finanze: Monsi (9 drfür, öpper enthaute)
 * Revisor: Nöbsu (übernoh vom Rouf, 8 drfür, 2 Enthautige)
 * Sekretär: Tinu (9 drfür, eine enthautet)

```
››› Pouse, Pizza bsteue, Rouchpouse
››› Rouf hässig, nei Rouf nid hässig, mou Rouf hässig, nei Rouf nid hässig wüu nid Protokoufüerer
››› Kommentar Rouf: "Fotze!"
…
››› Rouf het d'Pizza bsteut u zaut, augemeine Byfau mit Chlatsche u so
```


Usblick
-------

### Merch

*(siehe Diverses)*

### Uftritte

 * 22. Juli, Moonlight, irgendöpper macht dert es Fescht
    - Tinu fäuht, wär ersetzt ne?
 * Irgendeinisch no irgendwo im Wallis (Chlöti)
 * Groove Druids (?)
 * Vo Oute kenni o no e Band, wo gloub scho am Up In Smoke gspiut hei (Nöbsu)
    - Great Lady Under Earth
 * Thanrock Festival Ändi Juli (Chegi)
    - Aber aber Tinu isch ja nid da (aber evtl. nächscht Jahr)
 * Revolutionäri Idee: Säuber es Konzärt organisiere :-D
    - Je nachdäm, wi's mit de Ufnahme steit, Demos a Lokau schicke
    - Ah ja, apropos—

### Ufnahme

Realistisch das Jahr, oder?

### Mitgliederbyträg

 * Gmeind het erwähnt wäg de Miet- u Elektrizitätschöschte

```
››› Ässenspouse
››› Speter no Rouchpouse (i weiss o nid, es isch hie nid aues chronologisch)
```

 * Vorschlag Chegi: Resärve ufboue, damit mir di erschte paar Miete chöi zahle?
 * Mir müesse zersch mau bestimme, wi me's wott mache/finanziere
    - Multi-Tier (aktiv/passiv)
       * Definition:
          - "Aktiv" = Stromverbraucher
          - "Gönner" = aui angere (dörfe aber o meh zahle ^^)
       * Agnoh (6 drfür, 4 drgäge)
          - *(aber d'Definition isch o ersch när cho)* ¯\\(°\_o)/¯
    - Michu u Nöbsu im Usschuss für Namensgäbig
    - Nöii Byträg:
       * Biuig isch 5.--: Agnoh
       * Tüür isch 10.--: Agnoh

### Statute

Müesse mer mau ändere
 * Konflikt: Vizepräsi == Protokoufüehrer
 * Agnoh: Monsi überarbeitet mau d'Statute

Divers
------

### Rouf: Nöie Fernseh

Vom Rouf sym Grossvatter, dä het sogar Ton!
 * Solang dr aut nid z'Baggwiu ir WG landet: Agnoh (aui drfür)

### Rouf: Frequänzweiche-Budget

Frequänzweiche isch wider kaputt, bruche e Nöii.

 * Gägevorschlag Chlöti: Bruche mer ihri PAs mit integrierter Frequänzweiche
 * Choufe?
    - 3 drfür
    - 1 drgäge
    - 6 enthaute sech

### Rouf: Ufnahme

Müesse mer öppis bestimme, wäge potentieue Plugin-Chöschte für Ardour?

 * Augemein-Konsens: Nei
 * Mir luege mau, was mir bruche, u faus es faht afa choschte, luege mer de denn.
 * Abmische mache mer mau säuber, Mastering chöi mer de separat luege

### Rouf: Versicherig

Bändruum het mittlerwyle viu wärtvous Züg drinne. Bruche mir e Versicherig?

Konsens: Mir ändere nüt (jede luegt eifach säuber mit syre Husratsversicherig)

### Michu: Merch

 * Bierdose mit Bändlogo
    - Hei's versuecht mit Spraye/Amale, aber het nid so guet funktioniert
 * T-Shirt
    - Chlöti: Rohi T-Shirt bsteue
       * Instabil: Fairtrade (aber choschtet chly viu)
       * ÜBL: Brocki-T-Shirt
 * Flammewärfer
 * Nöbsu: Öppis zum Spile

```
››› 4 Bier wärde vom Rouf us em Chüuschrank ghout
```

 * BH? (O-Ton: "lieber e BH uf d'Bühni gschosse becho aus es Bierdösli")
 * (Demo-)Aubum-Art: Ufnahme emne Kolleg vom Michu shicke, är zeichnet's när

### Michu: Code ändere

 * Rouf änderet's uf ████
 * Michu mäudet's am Sändu, bis spätischtens 22. Juli **2023**

### Chlöti #2: Bauerpoint für di nächschti HV

### ???: HV oder GV?

 * Bestumme dass es itz e HV isch
    - Aber dr Protokoufüehrer het's verhängt, das z'notiere, drum itz haut hie
      im Usblick
       * Aber wiviu si eigentlech drfür gsy?

---

Ändi: 21:49
